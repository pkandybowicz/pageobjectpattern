package pageobjects;

import org.openqa.selenium.WebDriver;
import tests.BaseTest;

public class HomePage extends BasePage {

    public HomePage(WebDriver driverIn){
        this.driver = driverIn;
    }

    public void openPage(){
        driver.get(BASE_URL + "index.php");
    }

}
